 <?php
      include('Header.php');
    ?>
   <body>
  
 <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T" >STOCK REPORT</h4>
      <div class="form-group">
        <select style=""  id="OrderProduct" onchange='SearchByProduct()'></select>
        <input style="" type="Date" onchange="SearchByDate()" placeholder="Select Date" id="datepicker">
        <input style="" class="" type="Date" placeholder="Select Date" id="datepickerTo">
        <input type="button" class="btn btn-primary d-print-none" value="Go" onclick="TF()" style="width: 10%;padding: 10px">
      </div>
      <table class="wid table table-bordered table-hover" id="tabledata">
        <thead class="bg-primary text-white">
          <tr>
            <th scope="col">Invoice#</th> 
            <th scope="col">Product Name</th>
            <th scope="col">Sale Quantity</th>
            <th scope="col">Date</th>
          </tr>
        </thead>
        <tbody id="ReportTable">
          <?php
            include 'Connection.php';

             $query = "SELECT  invoicedetail.*, product.ProductName  From invoicedetail INNER JOIN product ON product.id = invoicedetail.product_id";
            
              if ($result=mysqli_query($con,$query))
                {  // Fetch one and one row
                while ($row=mysqli_fetch_assoc($result))
                {
              ?>
          <tr class="tr" >
              
              <td > <a href="ZN_Invoice.php?id=<?php echo $row['id'];?>"</a> <?php echo $row['id'];?></td> 
              <td ><?php echo $row['ProductName'];?>    </td> 
              <td ><?php echo $row['OrderQuantity']; ?> </td>
              <td ><?php echo $row['Date']?>            </td>
          </tr>     
            <?php
                  }
        } 
        ?>



        </tbody>
      </table>
    </div>
  </div>

  </body>

  <script type="text/javascript">
    var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "ap-GetData.php";
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();

      ajax.onreadystatechange = function display()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          var data = JSON.parse(this.responseText);
          console.log(data);
          var d = "";

          for (var i = 0; i<data.length ; i++)
          {
            var sr = data[i].id;
            var ProductName = data[i].ProductName;
          
              d +="<option > "+ ProductName + " </option>";
          }
          document.getElementById("OrderProduct").innerHTML = d;
        }
      }
  
    function TF()
    {
      var from = $("#datepicker").val();
      var to = $("#datepickerTo").val();
       var url = "SRTOFROM.php?from="+from+"&to="+to;
                            window.location.href = url;
    }
    function SearchByProduct() 
    {
      var e, input, filter, table, tr, td, i;
      e = document.getElementById("OrderProduct");//Getting Product From Select Tag
      
      input = e.options[e.selectedIndex].text;//Getting Product Through Option
     
      filter = input.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[3];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }

</script>
</html>