<?php
include('Header.php');
?>
<div class="wrapper">
    <div class="contain container mt-5">
	<h4 class="T" >PAYMENT RECEIPT</h4>
	
	<div class="Customerinfo" style="text-align: center;">
        <input type="Date" placeholder="Select Date" class=" in" id="i-datepicker">
        <div id="VendorName">
            <input list="vendor" sr="-1" placeholder="Select Vendor" id="vendors">
            <datalist id="vendor"  style="padding: 20px" class="in">
              <?php
                 $result = mysqli_query($con,"SELECT * FROM vendor order by VendorName asc");

                 while($row = mysqli_fetch_assoc($result))
                  {?>
                    <option id='<?php echo $row['id']?>' value='<?php echo $row['VendorName']?>' > <?php echo $row['VendorAddress']?> </option>
                    <?php
                  }
                 
            ?>
        </div>
        <input type="number" placeholder="Paid Amount" class=" in" id="i-PaidAmount">
        <input type="text" name="Cash/ChequeNo"class=" in" placeholder="Cash/Cheque No" id="i-receivedtype">
        <input type="text" name="bankname"class=" in" placeholder="Bank Name" id="i-bankname">
        <input type="Date" placeholder="Select Date" class=" in" id="i-bankdatepicker">
        <input type="text" name="being" class="in" id="i-being" placeholder="Being">
        <input type="submit" class="btn btn-primary in royalbutton" id="b-btnSave" value="SAVE" style="color: white">
	</div>
</div>
</div>
</body>

<script type="text/javascript">
    $(document).ready(function()
    {
        $("#b-btnSave").click(function AddDetail()
        {
            $(this).attr("disabled", true);
            var vendor_id = $("#vendor option[value='" + $('#vendors').val()+ "']").attr('id');
            var date = $("#i-datepicker").val();
            var PaidAmount = $("#i-PaidAmount").val();
            var receivedtype = $("#i-receivedtype").val();
            var bankname = $("#i-bankname").val();
            var bankdatepicker = $("#i-bankdatepicker").val();
            var being = $("#i-being").val();

            $.ajax({
                url:'PaidReceiptSave.php', //url from where we get data accesing DataBase
                    data: {vendor_id:vendor_id, PaidAmount:PaidAmount,receivedtype:receivedtype,date:date, bankdatepicker:bankdatepicker,being:being,bankname:bankname},//passing data to php page in which php will send data to Database
                    type: 'POST',
                    success:function(data){

                            callurl(data);
                            }
                            
                    });           
        });
    });
    function callurl(sr)
    {
        var url = "PrintPaidReceipt.php?id="+sr;
        window.location.href = url;
    }
</script>
<script>
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>


</html>