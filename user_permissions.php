<?php
      include('Header.php');
?>
<style type="text/css">
  p {
 border:1px solid #ccc;
 padding:0px;
 margin:0 0 10px;
 display:block; 
}
  label {
 border:2px solid #ccc;
 padding:0px;
 margin:0 0 10px;
 display:block; 
}
label:hover {
 background:#eee;
 cursor:pointer;
}
input{
  margin: 0px 10px 0px 10px;
}
</style>
  
   <div class="wrapper">
   <div class="container mt-5">

   <h4 class="T" >User Permisions</h4>

    <form action="save_user_permissions.php" method="POST">
      
      <div class="form-group">
        <div class="row">
          <div class="col-sm-4 mr-4">
          <label>Permission</label>
          </div>
          <div class="col-sm-4 mr-4">
          <label>Description</label>
          </div>
      </div>
        <input type="hidden" name="user_id" value="<?php echo $_GET['user_id']; ?>">
         <?php
         $user_id = $_GET["user_id"];
          include 'Connection.php';
          
          $user_rights = mysqli_query($con,"SELECT permissions FROM user WHERE sr=$user_id");
          while($per = mysqli_fetch_assoc($user_rights))
          {
            $permissions = json_decode($per['permissions']);
            $result = mysqli_query($con,"SELECT * FROM user_rights WHERE status=1 order by parent asc");
            while($row = mysqli_fetch_assoc($result))
        { ?>
        <div class="row">
          <div class="col-sm-4 mr-4">
          <p><input type="checkbox" class="" <?php if(in_array($row['id'], $permissions)){ echo "checked"; }  ?> name="right[]" value="<?php echo $row['id']; ?>" id="<?php echo $row['id']; ?>" ><?php echo $row["name"]; ?></p>
          </div>
          <div class="col-sm-4 mr-4">
          <p><?php echo $row["description"]; ?></p>
          </div>
      </div>
      <?php }}?>

      <button type="submit" class="btn-primary">Update</button>
      </div>
    </form>
    </div>
    </div>
  </body>
 
  
</html>
