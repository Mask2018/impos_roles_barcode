<?php include 'HeaderOnPrintPage.php';?>
<?php
 $ID=$_GET['id'];
$Date = '';
$query2 = "SELECT   invoicereturn.id,  invoicereturn.customer_id,  invoicereturn.Amount,  invoicereturn.date,  invoicereturn.salesman_id, customer.CustomerName,customer.CustomerAddress,customer.CustomerPhone From  invoicereturn INNER JOIN customer ON customer.id =  invoicereturn.customer_id WHERE  invoicereturn.id = $ID ";

if ($result2=mysqli_query($con,$query2))
  {  // Fetch one and one row
  while ($row2=mysqli_fetch_assoc($result2))
  {
     $CustomerAddress = $row2['CustomerAddress'];          
     $CustomerName = $row2['CustomerName'];
     $Date= $row2['date']; 
     $Amount=$row2['Amount'];
}
}

?>


<div class="Container" style="height: 100%;max-width: 785px; margin: 0px 0px 25px 0px;">
<div class="left_border" style="width: 10%;height: 100%; background-color: #afafb3;float: left;"></div>
<div class="SI" style="text-align: center;">
      <h4 style="font-weight: 900;background-color: #8c8c92;font-size: 50px;color: white;width: 840px"><b>SALE RETURN INVOICE</b></h4>
</div>

<div class="right_content" style="width: 85%;float: right;">
<div class="Top" style="line-height: 1;"><!--  d-none d-print-block -->
<div class="Company" style=" text-align: left; float: left; width: 60%;">

    <h4 style="text-decoration: underline; font-weight: 700; font-size: xx-large;margin:0px 0px 0px 150px" id="h-name"></h4>
    <!-- <h3> SALE INVOICE </h3>
    <h5>042 36367015/0303 4319049</h5> -->
    <h6 style="margin:0px 0px 0px 150px" id="h-address"></h6>
</div>
<!-- <div class="verticalline"></div> -->
<div class="CompanyDo" style="width: 100%; float: left;">
  <div class="lab" style="float: right;">
    <label style="font-weight: 700;">Ph:&nbsp </label><span id="s-ph1"></span><br>
    <label style="font-weight: 700;">Mobile:&nbsp</label><span id="s-mobile1"></span><span>/</span><span style="margin-left: 5px" id="s-mobile2">0334-0414001</span><br>
    <label style="font-weight: 700;">Web:&nbsp</label><span id="s-web"></span><br>
    <label style="font-weight: 700;">Email:&nbsp</label><span id="s-email"></span><br>
  </div>
  <!-- <h2>Deal In:</h2>
  <b><p>&nbsp Toyota, Suzuki, Honda<br>&nbsp Hundai  Body Parts & Lights.</p>
  <p>&nbsp Whole Sale & Retail.</p></b> -->
</div>
</div>
<p class="" style="background-color: black;color:white;margin: 0px 0px -12px 10px; width: fit-content;    overflow: auto; ">Account Detail</p>
<p class="" style="background-color: black;color:white;margin: -12px 100px 10px 0px; width: fit-content; float: right;">Voucher Detail</p>
<div class="accoutdetail" style="width: 100%;height:80px;border: 1px;border-style: solid;margin-bottom: 10px;">
   <div style="width: 68%; float: left; border-right:  1px solid black;max-height: -webkit-fill-available;">
    <label style="margin-top: 12px">Customer &nbsp</label> <?php echo $CustomerName; ?> <br>
   </div>
   <div style="float: left;line-height: 1">
    <div class="V-D" style="float: right;">
      <label style="font-weight: 700;">Voucher #:&nbspRInv-<b><?php echo $ID;?></b></label><br>
      <label style="font-weight: 700;">Date:&nbsp</label><span> <?php echo $Date;?></span>
    </div>
  </div>

</div>
  
  <div style="background-color: #ffffff">
  <table class="table"  style="font-size: 14px;" >
      <thead style="background: #8c8c92;font-weight: 800;border-bottom: 1px solid black;">
      <tr class="tr" >
        <th style=""> Sr#</th>
        <th style="font-weight: 800;"> Product Name  </th>
        <th style="font-weight: 800;"> Qty      </th>
        <th style="font-weight: 800;"> Rate     </th>
        <th style="font-weight: 800;"> Total</th>   
      </tr>
      </thead>
  <tbody id="OrderTable">
   <?php
   
        $query = "SELECT invoicereturndetail.*, product.ProductName  FROM invoicereturndetail INNER JOIN product ON product.id = invoicereturndetail.product_id WHERE invoice_return_id = $ID order by invoicereturndetail.id asc";

  if ($result=mysqli_query($con,$query))
    {  // Fetch one and one row
    while ($row=mysqli_fetch_assoc($result))
      {
  ?>
         <tr class="tr" >
              <td ></td>
              <td ><?php echo $row['ProductName'];?>  </td> 
              <td ><?php echo $row['qty'];?></td> 
              <td ><?php echo $row['rate']; ?>    </td>
              <td ><?php echo $row['total']?>   </td> 
          </tr>     
       <?php
    }
  }

  ?>
  </tbody>
</table>
</div>
<!--  -->

<div style="width: 30%;margin-top: 10px;float: right; border:1px solid black;">

     <div style="background-color: grey; border-bottom: 1px solid black"><label>Totals </label><b style="float: right;margin-right: 10px"><?php echo $Amount;?></b></div>
     
</div>
<?php require ('InvoiceReportFooter.php');?>
</div>
</div>
</body>
<script type="text/javascript">
    var table = $('#OrderTable');
    var tablerow = $('#OrderTable tr').length;
    var Row = '';
    if(tablerow < 10)
    {
     var row = 10-tablerow;
      
     var  a = 1;
      while(a<row)
      {
          Row += "<tr style='border:none'>";
            Row +="<td style='border-bottom:none;border-top:none'></td>";
            Row +="<td style='border-bottom:none;border-top:none'></td>";
            Row +="<td style='border-bottom:none;border-top:none'></td>";
            Row +="<td style='border-bottom:none;border-top:none'></td>";
          Row +="</tr>";
        a=  a+1;
      }
      table.append(Row);
    }


</script>
<script type="text/javascript">
    var ajax = new XMLHttpRequest();
    var method = "Get";
    var url = "get_company_detail.php";
    var asyn = true;
    //Ajax open XML Request
    ajax.open(method,url,asyn);
    ajax.send();

    ajax.onreadystatechange = function displayCustomer()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            var data = JSON.parse(this.responseText);
            console.log(data);
            var d = "";

            for (var i = 0; i<data.length ; i++)
            {
                var sr = data[i].sr;
                var name = data[i].name;
                var address = data[i].address;
                var phone1 = data[i].phone1;
                var phone2 = data[i].phone2;
                var mobile1 = data[i].mobile1;
                var mobile2 = data[i].mobile2;
                var fax = data[i].fax;
                var web = data[i].web;
                var email = data[i].email;
                var facebook = data[i].facebook;
                var slogan = data[i].slogan;
                
                
            }
            document.getElementById("h-name").innerHTML = "";
            document.getElementById("h-address").innerHTML = "";
            document.getElementById("s-email").innerHTML = email;
            document.getElementById("s-web").innerHTML = web;
            document.getElementById("s-mobile2").innerHTML = mobile2;
            document.getElementById("s-mobile1").innerHTML = mobile1;
            document.getElementById("s-ph1").innerHTML = phone1;
            
        }
    }

function printPage() {
    window.print();
    window.location.href = "invoice.php";
}
</script>

</html>
