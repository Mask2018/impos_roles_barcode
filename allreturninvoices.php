<?php
      include('Header.php');
    ?>
<body>
    <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T">Sale Return Reports</h4>
   
      <table class="wid table table-bordered table-hover table-fixed" id="tabledata">
        <thead class="bg-primary text-white" >
          <tr>
            <th scope="col" >Invoice#</th> 
            <th scope="col">Cutomer Name</th>
            <th scope="col">Date</th>
            <th scope="col">Total Amount</th>
          </tr>
        </thead>
        <tbody id="ReportTable">

      <?php
      include 'Connection.php';
         $query ="SELECT invoicereturn.*, customer.CustomerName FROM invoicereturn INNER JOIN customer ON customer.id = invoicereturn.customer_id";
         
        if ($result=mysqli_query($con,$query))
        {  // Fetch one and one row
          while ($row=mysqli_fetch_assoc($result))
          {

            ?>
          <tr class="tr" >
              
              <td > <a href="view_returninvoice.php?id=<?php echo $row['id'];?>"</a> <?php echo $row['id'];?></a></td> 
              <td ><?php echo $row['CustomerName'];?></td> 
              <td ><?php echo $row['date']; ?>  </td>
              <td ><?php echo $row['amount']?>  </td>
          </tr>
        <?php
            }
          } 
          ?>
        </tbody>
      </table>
    </div>
    </div>
  </body>

  <script type="text/javascript">
    function TF()
    {
      var from = $("#datepicker").val();
      var to = $("#datepickerTo").val();
       var url = "dailyTOFROM.php?from="+from+"&to="+to;
                            window.location.href = url;
    }
    
    function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    function SearchByName() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("SearchByName");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>