    <?php
      include('Header.php');
    ?>
<body>
    <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T">Sale Reports</h4>
      <div class="form-group">
          <input type="text" name="Name" onkeyup="SearchByName()" placeholder="SearchByName" id="SearchByName">
      </div>
      <table class="wid table table-bordered table-hover" id="tabledata">
        <thead class="bg-primary text-white">
          <tr>
            <th scope="col">Invoice#</th> 
            <th scope="col">Cutomer Name</th>
            <th scope="col">Date</th>
            <th scope="col">Total Amount</th>
            <th scope="col">Paid Amount</th>
              <th scope="col">Profit</th>
          </tr>
        </thead>
        <tbody id="ReportTable">
          
        </tbody>
      </table>
      <div id="Profit">
       <label>Profit:</label> <span id="itemprofit"></span><br>
        <label>Expness:</label><span id="expness">
          
        </span><br>
        <label>Net Profit:</label> <span id="TotalProf"></span>
      </div>
    </div>
    </div>
  </body>
  <script type="text/javascript">
      var from = getUrlVars()["from"];
      function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
      });
      return vars;
      }
      var to = getUrlVars()["to"];
      function getUrlVars() {
      var vars = {};
      var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
      vars[key] = value;
      });
      return vars;
      }
      var TP = 0;
      var Row = '';
      var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "Report-DisplayDataToFrom.php?from="+from+"&to="+to;//Invoice == Report
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();
      //ajax call for display
      ajax.onreadystatechange = function ReportDisplay()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          var data = JSON.parse(this.responseText);
          console.log(data);
          var d = "";
          for (var i = 0; i<data.length ; i++)
          {
            var sr = data[i].id;
            
            var CustomerName = data[i].CustomerName;    //InvoiceNo,CustomerAddress,Amount,Date,Paid
            var TotalAmount = data[i].Amount;
            var DDY = data[i].Date;
            var PaidAmount = data[i].Paid;
            var Profit = data[i].TotalProfit;
            d += "<tr data-id='"+sr+"'>";
              d +="<td data-target='Invoice' class='CustomerName'><a href='ZN_Invoice.php?id="+sr+"'>"+ sr +" </a></td>";
              d +="<td data-target='CustomerName' id='CustomerName' class='CustomerPhone' >"+ CustomerName + "</td>";
              d +="<td data-target='DDY' id='DDY' class='CustomerAddress' >"+ DDY + "</td>";
              d +="<td data-target='TotalAmount' id='TotalAmount' class='TotalAmount' >"+ TotalAmount + "</td>";
              d +="<td data-target='PaidAmount' id='PaidAmount' class='Balance' >"+ PaidAmount + "</td>";
               d +="<td data-target='PaidAmount' id='PaidAmount' class='Balance' >"+ Profit + "</td>";
              /*d +="<td> <a href='#' data-role='update' data-id='"+CustomerSr+"'>Update</a> </td>";Calling Model Through JS Function
              d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";*/
            d +="</tr>";
            TP = parseFloat(TP)+parseFloat(Profit);
		TP=TP.toFixed(2);
          }
          document.getElementById("ReportTable").innerHTML = d;

           var table = $("#ReportTable");
            Row += "<tr>";
                Row +="<td data-target='type' ></td>";
                Row +="<td data-target='Dat' ></td>";
                Row +="<td data-target='num' ></td>";
                Row +="<td data-target='num' ></td>";
                Row +="<td data-target='Debit' id='Debit' ><b></b></td>";
                Row +="<td data-target='Credit' id='Credit' ><b>"+TP +"</b></td>";
                //d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";
                Row +="</tr>";
                table.append(Row);
                document.getElementById("itemprofit").innerHTML = TP;
                getexpness(TP);
        }
      }
     function getexpness(TP)
      {

      var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "getexpnessdisplay.php?datefrom="+from+"&dateto="+to;//Invoice == Report
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();

      ajax.onreadystatechange = function ReportDisplay()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          var data = JSON.parse(this.responseText);
          console.log(data);
          var d = "";
          for (var i = 0; i<data.length ; i++)
          {
           var expness = data[i].Price;
          }
          document.getElementById("expness").innerHTML = expness;
          document.getElementById("TotalProf").innerHTML = (parseFloat(TP)-parseFloat(expness)).toFixed(2) ;
          
        }
      }
      }
      function SearchByName() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("SearchByName");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  </script>
