 <?php
      include('Header.php');
    ?>
   <body>
  
 <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T" id="h-date"></h4>
      <table class="wid table table-bordered table-hover" id="tabledata">
        <thead class="bg-primary text-white">
          <tr>
            <th scope="col">Product Name</th>
            <th scope="col">Order Qty</th>
            <th scope="col">Return Qty</th>
            <th scope="col">Sold Qty</th>
            <th scope="col">UnitRate</th>
            <th scope="col">ProductRate</th> 
          </tr>
        </thead>
        <tbody id="ReportTable"></tbody>
      </table>
      <div style="margin-top: 5px; float: right;">
          <b class="col-sm2"> Total Amount:</b>
          <b class="col-sm2" id="b-totalamount"></b>
          <br>
          <b class="col-sm2">  Credit:</b>
          <b class="col-sm2" id="b-credit"></b>    
          
           <br>
          <b class="col-sm2">  Recovery:</b>
          <b class="col-sm2" id="b-recovery"></b>    
          <br>
          <b class="col-sm2"> Expense:</b>
          <b class="col-sm2" id="b-expense"></b>
          <br>
          <b class="col-sm2"> Net Cash:</b>
          <b class="col-sm2" id="b-receivedamount"></b>         
      </div>
    </div>
  </div>

  </body>
  <script type="text/javascript">
  var date = getUrlVars()["date"];
  function getUrlVars() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
  vars[key] = value;
  });
  return vars;
  }
  document.getElementById("h-date").innerHTML = date;
      
      var EPrice = 0;
      var ReceivedAmount = 0;
      var Totals = 0;
      var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "daygetdata.php?date="+date;//Invoice == Report
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();
      //ajax call for display
      ajax.onreadystatechange = function ReportDisplay()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          /*var data = JSON.parse(this.responseText);
          */
          var datas=jQuery.parseJSON(this.response);
            
          /*console.log(data);*/
          var data = datas[0];
          var data1 = datas[1];
           var data2 = datas[2];
          var data3 = datas[3];
          var d = "";
          var ret = 0;
          for (var i = 0; i<data.length ; i++)
          {
            /*ProductName,OrderQuantity,UnitRate,ProductRate,ProductName*/
            var product_id = data[i].sa_id;
            var ProductName = data[i].name;    //InvoiceNo,CustomerAddress,Amount,Date,Paid
            var OrderQuantity = data[i].OrderQuantity;
            /*var UnitRate = data[i].UnitRate;*/
            var ProductRate = data[i].ProductRate;
            ProductRate = parseFloat(ProductRate);

            var UnitRate = (ProductRate.toFixed(2))/OrderQuantity;

            UnitRate = parseFloat(UnitRate).toFixed(2);
            var r_qty = 0;
            var sold = OrderQuantity;
          /*For Return Invoice Start*/
           for (var j = 0; j<data1.length ; j++)
            {
              var r_product = data1[j].re_id;
              if (r_product == product_id) 
              {
                r_qty = data1[j].qty;
                sold = OrderQuantity-r_qty;
                ProductRate = sold*UnitRate;
              }
            }   
          /*For Return Invoice*/
            d += "<tr data-id='"+ProductName+"'>";
              d +="<td data-target='Invoice' class='CustomerName'>"+ ProductName +"</td>";
              d +="<td data-target='CustomerName' id='CustomerName' class='CustomerPhone' >"+ OrderQuantity + "</td>";
               d +="<td data-target='CustomerName' id='CustomerName' class='CustomerPhone' >"+ r_qty + "</td>";
               d +="<td data-target='CustomerName' id='CustomerName' class='CustomerPhone' >"+ sold + "</td>";
              d +="<td data-target='DDY' id='DDY' class='CustomerAddress' >"+ UnitRate + "</td>";
              d +="<td data-target='DDY' id='DDY' class='CustomerAddress' >"+ ProductRate.toFixed(2); + "</td>";
              /*d +="<td> <a href='#' data-role='update' data-id='"+CustomerSr+"'>Update</a> </td>";Calling Model Through JS Function
              d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";*/
            d +="</tr>";
          }
           for (var x = 0; x<data2.length ; x++)
            {
              EPrice = data2[x].Price;
            }

            for (var y = 0; y<data3.length ; y++)
            {
              ReceivedAmount = data3[y].ReceivedAmount;
		if (EPrice != null) {
                   ReceivedAmount = parseFloat(ReceivedAmount);
                }
		else
		{
		ReceivedAmount = 0;
               
		}
              
		
            
		}

          document.getElementById("ReportTable").innerHTML = d;
          var Row = '';
          var sq = 0;
          var ur = 0;
          var pr = 0;
          
          var TotalRows = document.getElementById("ReportTable").rows.length;

        for( i = 0; i<TotalRows; i++)
        {
           var SQ = document.getElementById("ReportTable").rows[i].cells.item(1).innerHTML;
           var UR = document.getElementById("ReportTable").rows[i].cells.item(2).innerHTML;
           var PR = document.getElementById("ReportTable").rows[i].cells.item(3).innerHTML;
           var TotalR = document.getElementById("ReportTable").rows[i].cells.item(5).innerHTML;
            
            sq = parseFloat(SQ)+sq;
            ur = parseFloat(UR)+ur;
            pr = parseFloat(PR)+pr;
            Totals = parseFloat(Totals)+parseFloat(TotalR);
        }
        var table = $("#ReportTable");
            Row += "<tr>";
                Row +="<td data-target='num' ></td>";
                Row +="<td data-target='Debit' id='Debit' ><b>"+ sq + "</b></td>";
                Row +="<td data-target='Credit' id='Credit' ><b>"+ur  + "</b></td>";
                Row +="<td data-target='Credit' id='Credit' ><b>"+pr  + "</b></td>";
                Row +="<td data-target='Credit' id='Credit' ><b></b></td>";
                Row +="<td data-target='Credit' id='Credit' ><b>"+Totals.toFixed(2);  + "</b></td>";
                //d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";
                Row +="</tr>";
                table.append(Row);

                if (EPrice == null) {
                  EPrice = 0;
                }
               isNaN("ReceivedAmount")
                {
                 
                }
                 /*document.getElementById("b-totalamount").innerHTML = Totals;*/

                document.getElementById("b-recovery").innerHTML=ReceivedAmount;
                document.getElementById("b-expense").innerHTML=EPrice;
                Netcash(Totals,ReceivedAmount,EPrice);
 
        }
      }
      function Netcash(Totals,ReceivedAmount,EPrice)
      {
        var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "daygetinvoicedetail.php?date="+date;
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();
      //ajax call for display
      ajax.onreadystatechange = function ReportDisplay()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          var data = JSON.parse(this.responseText);
          
          var inv =  data[0];
          var ret = data[1];
          var d = "";
          for (var i = 0; i<inv.length ; i++)
          {
            /*ProductName,OrderQuantity,UnitRate,ProductRate,ProductName*/
            var Amount = inv[i].Amount;    //InvoiceNo,CustomerAddress,Amount,Date,Paid
            var Paid = inv[i].Paid;
            var ClaimAmount = inv[i].ClaimAmount;
            var Balance = inv[i].Balance;
            
          }
           for (var i = 0; i<ret.length ; i++)
          {
            /*ProductName,OrderQuantity,UnitRate,ProductRate,ProductName*/
            var ret_Amount = ret[i].Amount;    //retoiceNo,CustomerAddress,Amount,Date,Paid
          }
           var tot = Amount - ret_Amount;
          var NetCash = Amount-Balance-EPrice+ReceivedAmount;
          document.getElementById("b-totalamount").innerHTML = tot;
            document.getElementById("b-receivedamount").innerHTML = NetCash;
            document.getElementById("b-credit").innerHTML = tot;
        }
      }
      } 
  </script>
      
</html>