-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 08, 2020 at 09:39 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sweet_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `sr` int(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone1` varchar(250) NOT NULL,
  `phone2` varchar(250) NOT NULL,
  `mobile1` varchar(250) NOT NULL,
  `mobile2` varchar(250) NOT NULL,
  `fax` varchar(250) NOT NULL,
  `web` varchar(50) NOT NULL,
  `email` varchar(250) NOT NULL,
  `facebook` varchar(250) NOT NULL,
  `slogan` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(50) NOT NULL,
  `CustomerName` varchar(250) NOT NULL,
  `CustomerPhone` varchar(250) NOT NULL,
  `CustomerAddress` varchar(250) NOT NULL,
  `TotalAmount` double NOT NULL,
  `PaidAmount` double NOT NULL,
  `Balance` double NOT NULL,
  `region` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customerledger`
--

CREATE TABLE `customerledger` (
  `id` int(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `invoice_type` varchar(50) NOT NULL,
  `invoice_id` int(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `debit` double NOT NULL DEFAULT '0',
  `credit` double NOT NULL DEFAULT '0',
  `balance` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `expense`
--

CREATE TABLE `expense` (
  `id` int(50) NOT NULL,
  `expense` varchar(250) DEFAULT NULL,
  `Description` varchar(250) DEFAULT NULL,
  `Price` double NOT NULL,
  `Dat` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(50) NOT NULL,
  `customer_id` int(50) NOT NULL,
  `Amount` double NOT NULL,
  `Date` date NOT NULL,
  `Paid` double NOT NULL,
  `ClaimAmount` double NOT NULL,
  `Balance` double NOT NULL,
  `TotalProfit` double NOT NULL,
  `salesman_id` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoicedetail`
--

CREATE TABLE `invoicedetail` (
  `id` int(50) NOT NULL,
  `invoice_id` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `OrderQuantity` int(50) NOT NULL,
  `UnitRate` double NOT NULL,
  `ProductRate` double NOT NULL,
  `Date` date NOT NULL,
  `ItemProfit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoicereturn`
--

CREATE TABLE `invoicereturn` (
  `id` int(50) NOT NULL,
  `customer_id` int(50) NOT NULL,
  `amount` double NOT NULL,
  `salesman_id` int(50) NOT NULL,
  `date` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoicereturndetail`
--

CREATE TABLE `invoicereturndetail` (
  `id` int(50) NOT NULL,
  `invoice_return_id` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `rate` double NOT NULL,
  `total` double NOT NULL,
  `qty` int(50) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ledger`
--

CREATE TABLE `ledger` (
  `id` int(50) NOT NULL,
  `date` date NOT NULL,
  `type` varchar(50) NOT NULL,
  `number` int(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `debit` double NOT NULL DEFAULT '0',
  `credit` double NOT NULL DEFAULT '0',
  `balance` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `paidreceipt`
--

CREATE TABLE `paidreceipt` (
  `id` int(50) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `PaidAmount` double NOT NULL,
  `rtype` varchar(250) NOT NULL,
  `Bankname` varchar(250) NOT NULL,
  `date` date NOT NULL,
  `cdate` date NOT NULL,
  `being` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(50) NOT NULL,
  `ProductName` varchar(50) NOT NULL,
  `qr_code` varchar(250) DEFAULT NULL,
  `BikeName` varchar(50) NOT NULL,
  `Price` double NOT NULL,
  `Quantity` double NOT NULL,
  `Sold` varchar(50) NOT NULL,
  `Stock` varchar(50) NOT NULL,
  `PurchasePrice` double NOT NULL,
  `Modal` varchar(250) NOT NULL,
  `PakingPrice` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `ProductName`, `qr_code`, `BikeName`, `Price`, `Quantity`, `Sold`, `Stock`, `PurchasePrice`, `Modal`, `PakingPrice`) VALUES
(1, 'black', 'Fa0516801893', '', 11, 1, '6', '-9', 12, '', 123),
(3, 'meiji Fu', '4902705127050', '', 120, 123, '0', '123', 100, '', 0),
(2, 'Rose Room', '6085010046587', '', 13, 1333, '0', '1333', 12, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `purchaseinvoice`
--

CREATE TABLE `purchaseinvoice` (
  `id` int(50) NOT NULL,
  `vendor_id` int(50) NOT NULL,
  `BillAmount` double NOT NULL,
  `Date` date NOT NULL,
  `Paid` double NOT NULL,
  `Balance` double NOT NULL,
  `Claim` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseinvoicedetail`
--

CREATE TABLE `purchaseinvoicedetail` (
  `id` int(50) NOT NULL,
  `purchase_invoice_no` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `ProductName` varchar(250) NOT NULL,
  `PurchaseQuantity` int(50) NOT NULL,
  `UnitRate` double NOT NULL,
  `ProductRate` double NOT NULL,
  `Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchaseta`
--

CREATE TABLE `purchaseta` (
  `sr` int(50) NOT NULL,
  `TotalAmount` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `purchasetemp`
--

CREATE TABLE `purchasetemp` (
  `id` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `Quantity` int(50) NOT NULL,
  `Price` double NOT NULL,
  `Total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(50) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `CustomerName` varchar(250) NOT NULL,
  `ReceivedAmount` double NOT NULL,
  `rtype` varchar(250) NOT NULL,
  `Bankname` varchar(250) NOT NULL,
  `dat` date NOT NULL,
  `cdate` date NOT NULL,
  `being` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rta`
--

CREATE TABLE `rta` (
  `sr` int(11) NOT NULL,
  `TotalAmount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rtemp`
--

CREATE TABLE `rtemp` (
  `id` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `qty` int(11) NOT NULL,
  `rate` float NOT NULL,
  `total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salesman`
--

CREATE TABLE `salesman` (
  `id` int(50) NOT NULL,
  `name` varchar(250) NOT NULL,
  `address` varchar(250) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `sales` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ta`
--

CREATE TABLE `ta` (
  `Sr` int(50) NOT NULL,
  `TotalAmount` double NOT NULL,
  `TotalProfit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tem`
--

CREATE TABLE `tem` (
  `id` int(50) NOT NULL,
  `product_id` int(50) NOT NULL,
  `Quantity` int(50) NOT NULL,
  `Rate` double NOT NULL,
  `Total` double NOT NULL,
  `Profit` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tem`
--

INSERT INTO `tem` (`id`, `product_id`, `Quantity`, `Rate`, `Total`, `Profit`) VALUES
(16, 2, 3, 39, 39, 3),
(17, 3, 3, 360, 360, 60);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `sr` int(250) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `contactno` varchar(250) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `address` varchar(250) DEFAULT NULL,
  `status` varchar(250) NOT NULL,
  `permissions` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`sr`, `username`, `password`, `contactno`, `date`, `address`, `status`, `permissions`) VALUES
(2, 'Adeel', '111', '123', '2020-07-07 17:10:41', '123', 'User', '[\"6\",\"15\",\"14\",\"12\",\"2\",\"11\",\"24\",\"23\",\"17\",\"16\",\"4\",\"7\",\"8\"]');

-- --------------------------------------------------------

--
-- Table structure for table `user_rights`
--

CREATE TABLE `user_rights` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `parent` varchar(250) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_rights`
--

INSERT INTO `user_rights` (`id`, `name`, `parent`, `description`, `status`) VALUES
(1, 'Product', 'Product', 'Can access product tab', 1),
(2, 'Add Product', 'Product', 'Can add new product', 1),
(3, 'User', 'User', 'Can View Users', 1),
(4, 'Add User', 'User', 'Can add Users', 1),
(5, 'Customer', 'Customer', 'Can View Customers', 1),
(6, 'Add Customer', 'Customer', 'Can add Customers', 1),
(7, 'Vendor', 'Vendor', 'Can View Vendors', 1),
(8, 'Add Vendor', 'Vendor', 'Can add Vendors', 1),
(9, 'Expense', 'Expense', 'Can View Expenses', 1),
(10, 'Add Expense', 'Expense', 'Can add Expenses', 1),
(11, 'Paid Receipt', 'Receipt', 'Can Generate Paid Reciept', 1),
(12, 'Invoices Tab', 'Invoice', 'Can Access Sale Invoice', 1),
(13, 'Sale Invoice', 'Invoice', 'Can Generate Sale Invoice', 1),
(14, 'Purchase Invoice', 'Invoice', 'Can Generate Purchase Invoice', 1),
(15, 'Return Invoice', 'Invoice', 'Can Generate Return Invoice', 1),
(16, 'Reports', 'Reports', 'Can Access Reports tab', 1),
(17, 'Expense Reports', 'Reports', 'Can Access Expense Reports', 1),
(18, 'Paid Receipt Reports', 'Reports', 'Can Access Paid Receipt Reports', 1),
(19, 'Profit/Loss', 'Reports', 'Can see Profit/Loss', 1),
(20, 'Reports', 'Reports', 'Can Access Reports tab', 1),
(21, 'Purchase Reports', 'Reports', 'Can Access Purchase Reports', 1),
(22, 'Sale Reports', 'Reports', 'Can Access Sale Reports', 1),
(23, 'Sale Summery', 'Reports', 'Can View and print sale summary', 1),
(24, 'Return Invoices Reports', 'Reports', 'Can Access Return Invoice Reports', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `VendorName` varchar(50) NOT NULL,
  `VendorAddress` varchar(50) NOT NULL,
  `VendorPhone` varchar(50) NOT NULL,
  `id` int(50) NOT NULL,
  `TotalAmount` double NOT NULL,
  `PaidAmount` double NOT NULL,
  `Balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendorledger`
--

CREATE TABLE `vendorledger` (
  `id` int(50) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `vendorname` varchar(250) NOT NULL,
  `dat` date NOT NULL,
  `typ` varchar(50) NOT NULL,
  `num` double NOT NULL,
  `description` varchar(250) NOT NULL,
  `debit` double NOT NULL,
  `credit` double NOT NULL,
  `balance` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`sr`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerledger`
--
ALTER TABLE `customerledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `expense`
--
ALTER TABLE `expense`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoicedetail`
--
ALTER TABLE `invoicedetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoicereturn`
--
ALTER TABLE `invoicereturn`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoicereturndetail`
--
ALTER TABLE `invoicereturndetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `paidreceipt`
--
ALTER TABLE `paidreceipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductName`),
  ADD KEY `sr` (`id`);

--
-- Indexes for table `purchaseinvoice`
--
ALTER TABLE `purchaseinvoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`id`);

--
-- Indexes for table `purchaseinvoicedetail`
--
ALTER TABLE `purchaseinvoicedetail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchaseta`
--
ALTER TABLE `purchaseta`
  ADD PRIMARY KEY (`sr`);

--
-- Indexes for table `purchasetemp`
--
ALTER TABLE `purchasetemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rtemp`
--
ALTER TABLE `rtemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salesman`
--
ALTER TABLE `salesman`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ta`
--
ALTER TABLE `ta`
  ADD PRIMARY KEY (`Sr`);

--
-- Indexes for table `tem`
--
ALTER TABLE `tem`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `sr` (`sr`);

--
-- Indexes for table `user_rights`
--
ALTER TABLE `user_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique` (`id`);

--
-- Indexes for table `vendorledger`
--
ALTER TABLE `vendorledger`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `sr` int(250) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customerledger`
--
ALTER TABLE `customerledger`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expense`
--
ALTER TABLE `expense`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoicedetail`
--
ALTER TABLE `invoicedetail`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoicereturn`
--
ALTER TABLE `invoicereturn`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoicereturndetail`
--
ALTER TABLE `invoicereturndetail`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ledger`
--
ALTER TABLE `ledger`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `paidreceipt`
--
ALTER TABLE `paidreceipt`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `purchaseinvoice`
--
ALTER TABLE `purchaseinvoice`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchaseinvoicedetail`
--
ALTER TABLE `purchaseinvoicedetail`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchaseta`
--
ALTER TABLE `purchaseta`
  MODIFY `sr` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchasetemp`
--
ALTER TABLE `purchasetemp`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rtemp`
--
ALTER TABLE `rtemp`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `salesman`
--
ALTER TABLE `salesman`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ta`
--
ALTER TABLE `ta`
  MODIFY `Sr` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tem`
--
ALTER TABLE `tem`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `sr` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_rights`
--
ALTER TABLE `user_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendorledger`
--
ALTER TABLE `vendorledger`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
