<?php
      include('Header.php');
    ?>
<body>
    <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T">Sale Reports</h4>
      <div class="form-group">
          <input type="Date" class="datepicker" onchange="SearchByDate()" placeholder="Select Date" id="datepicker">
          <input type="Date" class="datepicker" placeholder="Select Date" id="datepickerTo">
          <input type="text" name="Name" onkeyup="SearchByName()" placeholder="SearchByName" id="SearchByName" style="width: 20%;padding: 10px">
          <input type="button" class="btn btn-primary d-print-none" value="Go" onclick="TF()" style="width: 10%;padding: 10px"> 
      </div>
      <table class="wid table table-bordered table-hover table-fixed" id="tabledata">
        <thead class="bg-primary text-white" >
          <tr>
            <th scope="col" >Invoice#</th> 
            <th scope="col">Cutomer Name</th>
            <th scope="col">Date</th>
            <th scope="col">Total Amount</th>
            <th scope="col">Profit</th>
          </tr>
        </thead>
        <tbody id="ReportTable">
          
<?php
       
    include 'Connection.php';
      $query = "SELECT  invoice.id, invoice.customer_id, invoice.Amount, invoice.date,invoice.TotalProfit, invoice.salesman_id, customer.CustomerName,customer.CustomerAddress  From invoice INNER JOIN customer ON customer.id = invoice.customer_id";

        if ($result=mysqli_query($con,$query))
        {  // Fetch one and one row
          while ($row=mysqli_fetch_assoc($result))
          {
            ?>
          <tr class="tr" >
              
              <td > <a href="ZN_Invoice.php?id=<?php echo $row['id'];?>" </a> <?php echo $row['id'];?></td> 

              <td ><?php echo $row['CustomerName'];?></td> 
              <td ><?php echo $row['date']; ?>          </td>
              <td ><?php echo $row['Amount']?>          </td>
              <td ><?php echo $row['TotalProfit']; ?>   </td>
 
          </tr>     
        <?php
          }
        } 
        ?>

        </tbody>
      </table>
    </div>
    </div>
  </body>

<!-- 
  <script type="text/javascript">
    //Display Function For Customer
      var ajax = new XMLHttpRequest();
      var method = "Get";
      var url = "Report-DisplayData.php";//Invoice == Report
      var asyn = true;
      //Ajax open XML Request
      ajax.open(method,url,asyn);
      ajax.send();
      //ajax call for display
      ajax.onreadystatechange = function ReportDisplay()
      {
        if(this.readyState == 4 && this.status == 200)
        {
          var data = JSON.parse(this.responseText);
          console.log(data);
          var d = "";
          for (var i = 0; i<data.length ; i++)
          {
            var sr = data[i].sr;
            var Invoice = data[i].InvoiceNo;
            var CustomerName = data[i].CustomerAddress;    //InvoiceNo,CustomerAddress,Amount,Date,Paid
            var TotalAmount = data[i].Amount;
            var DDY = data[i].Date;
            var PaidAmount = data[i].Paid;
            var Profit = data[i].TotalProfit;

            var ajax = new XMLHttpRequest();
		      var method = "Get";
		      var url = "Reportcn.php?name="+CustomerName;
		      var asyn = true;
		      //Ajax open XML Request
		      ajax.open(method,url,asyn);
		      ajax.send();
		      //ajax call for display
		      ajax.onreadystatechange = function ReportDisplay()
		      {
		        if(this.readyState == 4 && this.status == 200)
		        {
		          var data = JSON.parse(this.responseText);
		          console.log(data);
		          var d = "";
		          for (var i = 0; i<data.length ; i++)
		          {
		            CustomerName = data[i].CustomerName; 
		          }
		        }
		      }


            d += "<tr data-id='"+sr+"'>";
              d +="<td data-target='Invoice' class='CustomerName'><a href='Invoice_1.php?id="+sr+"'>"+ sr +" </a></td>";
              d +="<td data-target='CustomerName' id='CustomerName' class='CustomerPhone' >"+ CustomerName + "</td>";
              d +="<td data-target='DDY' id='DDY' class='CustomerAddress' >"+ DDY + "</td>";
              d +="<td data-target='TotalAmount' id='TotalAmount' class='TotalAmount' >"+ TotalAmount + "</td>";
              d +="<td data-target='PaidAmount' id='PaidAmount' class='Balance' >"+ PaidAmount + "</td>";
              d +="<td data-target='PaidAmount' id='PaidAmount' class='Balance' >"+ Profit + "</td>";
              /*d +="<td> <a href='#' data-role='update' data-id='"+CustomerSr+"'>Update</a> </td>";Calling Model Through JS Function
              d +="<td> <input type='button' id='"+CustomerName+"' Value='Delete' class='Delete'> </td>";*/
            d +="</tr>";
          }
          document.getElementById("ReportTable").innerHTML = d;
        }
      }
  </script>

-->

  <script type="text/javascript">
    function TF()
    {
      var from = $("#datepicker").val();
      var to = $("#datepickerTo").val();
       var url = "dailyTOFROM.php?from="+from+"&to="+to;
                            window.location.href = url;
    }
    
    function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[2];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    function SearchByName() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("SearchByName");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[1];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  </script>
<script>
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>