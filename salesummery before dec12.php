    <?php
      include('Header.php');
    ?>
<body>
    <div class="wrapper">
    <div class="container mt-5">
      <h4 class="T">Sale Summery</h4>
      <div class="form-group">
         <input type="Date" class="datepicker" onchange="SearchByDate()" placeholder="Select Date" id="datepicker">
         <!--  <input type="Date" class="datepicker" placeholder="Select Date" id="datepickerTo">
          <input type="button" class="btn btn-primary d-print-none" value="Go" onclick="TF()" style="width: 10%;padding: 10px">  -->
      </div>
      <table class="wid table table-bordered table-hover" id="tabledata">
        <thead class="bg-primary text-white" id="OrderTable">
          <tr>
           <th scope="col">Date</th>
            <th scope="col">Total Amount</th>
            <th scope="col">Rec. Amount</th>
            <th scope="col">Discount</th>
          </tr>
        </thead>
        <tbody id="ReportTable">
          <?php
       
    include 'Connection.php';
     $query = mysqli_query($con,"SELECT Date, SUM(Amount) as Amount, SUM(Paid) as Paid, SUM(ClaimAmount) as ClaimAmount FROM invoice GROUP BY Date");

        if ($query)
        {  // Fetch one and one row
          while ($row=mysqli_fetch_assoc($query))
          {
            ?>
          <tr class="tr" >
              
              <td > <a href="daysummery.php?date=<?php echo $row['Date'];?>" </a> <?php echo $row['Date'];?></td> 

             <!--  <td ><?php echo $row['Date'];?></td>  -->
              <td ><?php echo $row['Amount']; ?>          </td>
              <td ><?php echo $row['Paid']?>          </td>
              <td ><?php echo $row['ClaimAmount']; ?>   </td>
 
          </tr>     
        <?php
          }
        } 
        ?>
        </tbody>

  <script type="text/javascript">
   
    function TF()
    {
      var from = $("#datepicker").val();
      var to = $("#datepickerTo").val();
      var url = "summeryTOFROM.php?from="+from+"&to="+to;
      window.location.href = url;
    }

     function SearchByDate() 
    {
      var input, filter, table, tr, td, i;
      input = document.getElementById("datepicker");
      filter = input.value.toUpperCase();
      table = document.getElementById("tabledata");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  //Script is to Hovar/Mark opened page in navbar
    $(function(){
        $('a').each(function(){
            if ($(this).prop('href') == window.location.href) {
                $(this).addClass('active'); $(this).parents('li').addClass('active');
            }
        });
    });
</script>
</html>